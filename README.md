# modules_and_packages_2022_07_18_gr_3005

---

[video](https://youtu.be/UnxXr-PMRRc)

---

## Порядок пошуку модуля:

- поточний каталог
- список каталогів, які містяться в змінній середовища (якщо є) PYTHONPATH
- список каталогів залежний від процедури встановлення Python

> import sys
 
> sys.path

import <modile_1> [, <module_2> ...]

from <module_1> import <name(s)>

from <module_1>  import <name_1> as <alt_name_1> [, <name_2> as <alt_name_2> ...]

import <modole_name> as <alt_name>

'''try:

    import baz

except ImportError:

    print("")'''


dir()

